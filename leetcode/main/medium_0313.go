package main

import "fmt"

// 堆调整
func headAdjust(nums *[]int, position int) {
	if position < len(*nums) {
		// fmt.Println("position = ", position, "nums = ", *nums)
		left := position * 2
		right := position*2 + 1
		if right < len(*nums) && (*nums)[position] > (*nums)[right] {
			tmp := (*nums)[position]
			(*nums)[position] = (*nums)[right]
			(*nums)[right] = tmp
		}
		if left < len(*nums) && (*nums)[position] > (*nums)[left] {
			tmp := (*nums)[position]
			(*nums)[position] = (*nums)[left]
			(*nums)[left] = tmp
		}
		// 对堆数据进行递归调整
		headAdjust(nums, left)
		headAdjust(nums, right)
	}
}

func nthSuperUglyNumber(n int, primes []int) int {
	result := 0
	if n > 0 && len(primes) > 0 {
		// 存放所有的数据
		save := []int{}
		// 候选的数据
		candidate := []int{0, 1}
		// 已经使用过的数据
		used := make(map[int]bool)
		used[1] = true

		// 如果还没有找到数据的话，则进行训话查找
		for len(save) < n {
			// 添加进来一个元素
			save = append(save, candidate[1])
			mul := candidate[1]
			// 去掉当前的数据
			tmp := []int{0}
			tmp = append(tmp, candidate[2:]...)
			candidate = tmp
			for i := 0; i < len(primes); i++ {
				if _, ok := used[primes[i]*mul]; !ok {
					tmpcan := []int{0, primes[i] * mul}
					tmpcan = append(tmpcan, candidate[1:]...)
					candidate = tmpcan
					headAdjust(&candidate, 1)
					used[primes[i]*mul] = true
				}
			}
			headAdjust(&candidate, 1)
			fmt.Println(save, candidate)
		}
		result = save[len(save)-1]
		fmt.Println(save, candidate)
		fmt.Println(len(save), len(candidate))
	}
	return result
}

func main() {
	primes := []int{2, 11, 17, 19, 23, 29, 31, 41, 53, 59, 67, 73, 79, 89, 101, 127, 137, 139, 149, 163, 167, 179, 181, 191, 197, 223, 239, 241, 251, 263}
	fmt.Println(nthSuperUglyNumber(50, primes))
}
